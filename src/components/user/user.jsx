import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getUsers, deleteUser } from '../../store/actions/user';


class user extends Component {
    componentDidMount() {
        const { getUser } =this.props;
        getUser();
    }
    onDelete(id) {
        const { deleteUser } = this.props;
        deleteUser(id);
    }

    render() {
        const { users } =this.props;
        return(
            <div className="container">
                <ul>
                    {_.map(users,(user,i)=>{ return(<li key={i} onClick={ ()=>this.onDelete(user.id) }>{user.title}</li>) })}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteUser: (id) => { dispatch(deleteUser(id)) },
        getUser: () => { dispatch(getUsers()) }
    }
}
 
export default connect(mapStateToProps,mapDispatchToProps)(user);