import axios from 'axios';
import {
    GET_USER,
    DELETE_USER
} from './types';

export function getUsers () {
    return (dispatch) =>{
        axios.get(`https://jsonplaceholder.typicode.com/posts`)
        .then((response)=>{
            dispatch({
                type: GET_USER,
                payload: response.data
            })
        })
        .catch((error)=>{
            console.log('error in get user action function',error);
        });
    }
}
export function deleteUser (userId) {
    return (dispatch) =>{
        axios.delete(`https://jsonplaceholder.typicode.com/posts/${userId}`)
        .then(()=>{
            dispatch({
                type: DELETE_USER,
                payload: userId
            });
        })
        .catch((error)=>{
            console.log('error from delete user action function',error);
        });
    }
}