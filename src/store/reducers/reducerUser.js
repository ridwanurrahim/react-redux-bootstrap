import {
    DELETE_USER,
    GET_USER
} from '../actions/types';
import { updateObject } from './reducer';
import _ from 'lodash';

const initialState = {};

export default (state = initialState, action) => {

    switch (action.type) {
        case GET_USER: {
            const result = action.payload;
            return updateObject(state, result);
        }

        case DELETE_USER: {
            const result = action.payload;
            const newState = _.each(state, (value, key, obj) => {
                if (value.id === result) {
                  _.unset(obj, key);
                }
            });
            return updateObject(state, newState);
        }
        
        default:
            return state;
     }
 }; 
